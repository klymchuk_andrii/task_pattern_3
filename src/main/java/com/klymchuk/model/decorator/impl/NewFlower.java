package com.klymchuk.model.decorator.impl;

import com.klymchuk.model.bouquet.BouquetFactory;
import com.klymchuk.model.bouquet.FlowerType;
import com.klymchuk.model.bouquet.Flowers;
import com.klymchuk.model.decorator.BouquetDecorator;

public class NewFlower extends BouquetDecorator {
    public NewFlower(Flowers flowers){
        setFlowers(flowers);
        setAdditionalPrice(flowers.getPrice()*flowers.getCountOfFlower());
    }
}
