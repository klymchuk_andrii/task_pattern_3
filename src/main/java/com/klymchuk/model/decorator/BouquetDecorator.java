package com.klymchuk.model.decorator;

import com.klymchuk.model.bouquet.Bouquet;
import com.klymchuk.model.bouquet.BouquetImp;
import com.klymchuk.model.bouquet.Flowers;

import java.util.List;
import java.util.Optional;

public class BouquetDecorator implements Bouquet{
    private Bouquet bouquet = new BouquetImp();
    private double additionalPrice;

    public void setBouquet(Bouquet bouquet) {
        this.bouquet = bouquet;
    }

    public void setFlowers(Flowers flowers){
        bouquet.getFlowers().add(flowers);
    }

    public void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    @Override
    public double getAllPrice() {
        return bouquet.getAllPrice()+additionalPrice;
    }

    @Override
    public List<Flowers> getFlowers() {
        return bouquet.getFlowers();
    }
}
