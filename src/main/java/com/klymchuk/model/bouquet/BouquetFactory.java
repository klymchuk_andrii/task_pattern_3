package com.klymchuk.model.bouquet;

import com.klymchuk.model.bouquet.catalog.Narcissus;
import com.klymchuk.model.bouquet.catalog.PearlBouquet;

import javax.swing.*;

public class BouquetFactory {
    public Bouquet createBouquet(CatalogType catalogType){
        switch (catalogType){
            case PERL:
                return new PearlBouquet();
            case NARCISSUS:
                return new Narcissus();
            default:
                return new BouquetImp();
        }
    }
}
