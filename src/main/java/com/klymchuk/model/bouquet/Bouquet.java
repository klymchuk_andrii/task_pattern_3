package com.klymchuk.model.bouquet;

import java.util.List;

public interface Bouquet {
    double getAllPrice();
    public List<Flowers> getFlowers();
}
