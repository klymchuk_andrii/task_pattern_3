package com.klymchuk.model.bouquet;

public class Flowers {
    private FlowerType flowerType;
    private double price;
    private String color;
    private int countOfFlower;

    public Flowers(FlowerType flowerType, double price, String color, int countOfFlower) {
        this.flowerType = flowerType;
        this.price = price;
        this.color = color;
        this.countOfFlower = countOfFlower;
    }

    public FlowerType getFlowerType() {
        return flowerType;
    }

    public void setFlowerType(FlowerType flowerType) {
        this.flowerType = flowerType;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getCountOfFlower() {
        return countOfFlower;
    }

    public void setCountOfFlower(int countOfFlower) {
        this.countOfFlower = countOfFlower;
    }

    @Override
    public String toString() {
        return countOfFlower + " " +
                color + " " +
                flowerType +
                " price: " + price + "\n";
    }
}
