package com.klymchuk.model.bouquet.catalog;

import com.klymchuk.model.bouquet.Bouquet;
import com.klymchuk.model.bouquet.FlowerType;
import com.klymchuk.model.bouquet.Flowers;

import java.util.ArrayList;
import java.util.List;

public class PearlBouquet implements Bouquet {
    private List<Flowers> flowers;
    private final double price = 250;


    public PearlBouquet(){
        flowers = new ArrayList<>();
        flowers.add(new Flowers(FlowerType.ROSE,12,"red",20));
        flowers.add(new Flowers(FlowerType.ROSE,15,"white",1));
    }

    @Override
    public double getAllPrice() {
        return price;
    }

    public List<Flowers> getFlowers() {
        return flowers;
    }

    @Override
    public String toString() {
        return "Bouquet: " +
                flowers + "\n";
    }
}
