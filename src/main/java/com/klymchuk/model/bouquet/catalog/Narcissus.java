package com.klymchuk.model.bouquet.catalog;

import com.klymchuk.model.bouquet.Bouquet;
import com.klymchuk.model.bouquet.FlowerType;
import com.klymchuk.model.bouquet.Flowers;

import java.util.ArrayList;
import java.util.List;

public class Narcissus implements Bouquet {
    private List<Flowers> flowers;
    private final double price = 250;

    public Narcissus(){
        flowers = new ArrayList<>();
        flowers.add(new Flowers(FlowerType.NARCISSUS,8,"yellow",10));
        flowers.add(new Flowers(FlowerType.NARCISSUS,9,"white",10));
    }

    @Override
    public double getAllPrice() {
        return price;
    }

    public List<Flowers> getFlowers() {
        return flowers;
    }

    @Override
    public String toString() {
        return "Bouquet: " +
                flowers + "\n";
    }
}
