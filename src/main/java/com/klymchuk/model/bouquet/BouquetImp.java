package com.klymchuk.model.bouquet;

import java.util.ArrayList;
import java.util.List;

public class BouquetImp implements Bouquet {
    private List<Flowers> flowers = new ArrayList<>();

    @Override
    public double getAllPrice() {
        double price = 0;

        if (flowers.isEmpty()) {
            System.out.println("\nBouquet is empty");
        } else {
            for (Flowers f : flowers) {
                price += f.getPrice() * f.getCountOfFlower();
            }
        }

        return price;
    }

    public List<Flowers> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flowers> flowers) {
        this.flowers = flowers;
    }

    @Override
    public String toString() {
        return "Bouquet: " +
                flowers + "\n";
    }
}
