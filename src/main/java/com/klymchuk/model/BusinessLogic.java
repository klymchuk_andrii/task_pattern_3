package com.klymchuk.model;

import com.klymchuk.model.bouquet.*;
import com.klymchuk.model.decorator.BouquetDecorator;
import com.klymchuk.model.decorator.impl.NewFlower;

public class BusinessLogic {
    public static void main(String[] args) {
        Bouquet bouquet = new BouquetFactory().createBouquet(CatalogType.PERL);

        System.out.println(bouquet);

        BouquetDecorator bouquetDecorator = new BouquetDecorator();
        bouquetDecorator.setBouquet(bouquet);
        System.out.println("bouquet: "+bouquetDecorator.getAllPrice());

        Flowers flowers = new Flowers(FlowerType.NARCISSUS,10,"yellow",10);
        BouquetDecorator newFlower = new NewFlower(flowers);
        Flowers flowers1 = new Flowers(FlowerType.NARCISSUS,10,"yellow",10);
        BouquetDecorator newFlower2 = new NewFlower(flowers1);


        newFlower.setBouquet(bouquetDecorator);

        System.out.println("newFlower1: "+newFlower.getAllPrice());

        newFlower2.setBouquet(newFlower);

        System.out.println("newFlower2: "+newFlower2.getAllPrice());

        System.out.println(newFlower2);
    }
}
